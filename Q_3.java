/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab_8;

/**
 *
 * @author Afnan
 */
abstract interface Country {

    abstract public void setCountryName(String s);
}

abstract interface University {

    abstract public void setUniversityName(String s);
}

class Student implements Country, University {

    String countryName;
    String universityName;

    @Override
    public void setCountryName(String s) {
        this.countryName = s;
    }

    @Override
    public void setUniversityName(String s) {
        this.universityName = s;
    }

    void showDetails() {
        System.out.println("Country Name : " + this.countryName + "\n" + "University Name : " + this.universityName + "\n");
    }
}

public class Q_3 {

    public static void main(String[] args) {

        Student s1 = new Student();
        s1.setCountryName("Bangladesh");
        s1.setUniversityName("Eastern University");
        s1.showDetails();

        Student s2 = new Student();
        s2.setCountryName("Uganda");
        s2.setUniversityName("Eastern University");
        s2.showDetails();
    }
}
