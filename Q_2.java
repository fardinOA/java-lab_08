/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab_8;

/**
 *
 * @author Afnan
 */
abstract class Polygon {

    int noOfVertices;

    abstract void no_of_vertices(int a);

    void showTypeOfPloygon() {

        switch (this.noOfVertices) {
            case 4:
                System.out.println("Its a Rectangle");
                break;
            case 5:
                System.out.println("Its a Pentagon");
                break;
            default:
                System.out.println("Undefined Polygon");
        }
    }
}

class Rectangle extends Polygon {

    @Override
    void no_of_vertices(int a) {
        this.noOfVertices = a;
    }

}

class Pentagon extends Polygon {

    @Override
    void no_of_vertices(int a) {
        this.noOfVertices = a;
    }

}

public class Q_2 {

    public static void main(String[] args) {
        Pentagon p = new Pentagon();
        p.no_of_vertices(5);
        p.showTypeOfPloygon();

        Rectangle r = new Rectangle();
        r.no_of_vertices(4);
        r.showTypeOfPloygon();

        Rectangle r1 = new Rectangle();
        r1.no_of_vertices(6);
        r1.showTypeOfPloygon();
    }
}
